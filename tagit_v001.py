#!/usr/bin/env python
import numpy as np
import sys as sys
import itertools as it
import random as rd

def save_xyz(atom, xyz, file):
	"""
	Save XYZ file
	"""
	with open(sys.argv[1]+file, 'w') as f:
		f.write(str(len(atom)))
		f.write('\n')
		f.write(str(file))
		f.write('\n')
		for line in range(len(atom)):
			f.write(str(atom[line]) + '    ' + '{:18}'.format(format(xyz[line][0],'.18f')[:15]) + ' ' + '{:18}'.format(format(xyz[line][1],'.18f')[:15]) + ' ' + '{:18}'.format(format(xyz[line][2],'.18f')[:15]))
			f.write('\n')
	return

def list_product(list1,list2):
	"""
	Return list product of two lists
	"""
	list_temp=[]
	for i in list1:
		row_temp=[]
		try:
			for j in i:
				row_temp.append(j)
		except TypeError: row_temp.append(i)
		
		for k in list2:
			row_temp2 = row_temp[:]
			if k in row_temp2:
				continue
			else:
				row_temp2.append(k)
				list_temp.append(row_temp2)
	return list_temp
	
def rot_matrix(axis, angle):
    """
	Calculate 3D rotation matrix for rotation with respect to axis by angle
	in counterclockwise direction
    """
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    a = np.cos(angle/2.0)
    b, c, d = -axis * np.sin(angle/2.0)
    return np.array([[a*a + b*b - c*c - d*d, 2 * (b*c + a*d), 2 * (b*d - a*c)],[2 * (b*c - a*d), a*a + c*c - b*b - d*d, 2 * (c*d + a*b)],[2 * (b*d + a*c), 2 * (c*d - a*b), a*a + d*d - b*b - c*c]])

def random_rotation():
	"""
	Get the 3d rotation matrix of a random rotation around a random axis
	and a random angle
	"""
	axis_rand = []
	for i in range(3):
		axis_rand.append(rd.random()*2-1)
	angle_rand = rd.random()*2*np.pi
	rot = rot_matrix(axis_rand,angle_rand)
	return rot
	
# Define constants
conv_bohr = 0.52917721067 # conversion from bohr to angstrom; taken from https://en.wikipedia.org/wiki/Bohr_radius (05.12.2018)

# Read data (sys.argv[1] is the working directory, sys.argv[2] is the number of tags to add)
atom_mol = np.genfromtxt(sys.argv[1]+'tagit_molecule.xyz', delimiter=None, usecols = (0), skip_header = 2, dtype=str)
xyz_mol = np.genfromtxt(sys.argv[1]+'tagit_molecule.xyz', delimiter=None, usecols = (1, 2, 3), skip_header = 2) # unit in Angstrom
atom_tag = np.genfromtxt(sys.argv[1]+'tagit_tag.xyz', delimiter=None, usecols = (0), skip_header = 2, dtype=str)
xyz_tag = np.genfromtxt(sys.argv[1]+'tagit_tag.xyz', delimiter=None, usecols = (1, 2, 3), skip_header = 2) # unit in Angstrom
surf = np.genfromtxt(sys.argv[1]+'tagit_vdw.txt', delimiter=None, usecols = (0, 1, 2), skip_header = 1) # unit in Bohr
surf=surf*conv_bohr
n_tags=int(sys.argv[2])
tag_pos=range(len(surf))

for i in range(n_tags):
	if i < 1:
		pass
	else:
		tag_pos = list_product(tag_pos,range(len(surf)))

# Get center of tag molecule
tag_center = np.array([0.0,0.0,0.0])
# Necessary for single atom tags like noble gases
if atom_tag.shape == ():
	atom_tag = [atom_tag.tolist()]
	xyz_tag = [xyz_tag]
for i in range(len(atom_tag)):
	for j in range(3):
		tag_center[j] += xyz_tag[i][j]
tag_center /= len(atom_tag)

# Place tag in the center
for i in range(len(atom_tag)):
	xyz_tag[i] -= tag_center

# Place tag molecules on all possible positions
for k in range(len(tag_pos)):
	if n_tags == 1:
		disp_temp = surf[tag_pos[k]]
		tag_temp = np.dot(np.copy(xyz_tag),random_rotation())
		xyz_temp = np.copy(xyz_mol).tolist()
		atom_temp = np.copy(atom_mol).tolist()
		
		for i in range(len(atom_tag)):
			for j in range(3):
				tag_temp[i][j] += disp_temp[j]
			xyz_temp.append(tag_temp[i])
			atom_temp.append(atom_tag[i])
		save_xyz(atom_temp, xyz_temp, 'tagit_' + str(k) + '.xyz')
	else:
		xyz_temp = np.copy(xyz_mol).tolist()
		atom_temp = np.copy(atom_mol).tolist()
		for j in tag_pos[k]:
			disp_temp = surf[j]
			tag_temp = np.dot(np.copy(xyz_tag),random_rotation())
			
			for i in range(len(atom_tag)):
				for j in range(3):
					tag_temp[i][j] += disp_temp[j]
				xyz_temp.append(tag_temp[i])
				atom_temp.append(atom_tag[i])
		save_xyz(atom_temp, xyz_temp, 'tagit_' + str(k) + '.xyz')
save_xyz(atom_temp, xyz_temp, 'tagit_molecule.xyz')

