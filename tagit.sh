#!/bin/bash
readonly ARGS="$@"

function get_args () {
	nproc=1 # number of processors
	mem=1000 # memory
	tags=1 # number of tags
	charge=0
	spin=1
	
    # Parse the arguments
    local OPTIND OPTARG flag t c m
    while getopts ':t:c:m:' flag; do
        case $flag in
            t) tags=${OPTARG};;
			c) charge=${OPTARG};;
			m) spin=${OPTARG};;
            *)
              usage
              exit
              ;;
        esac
    done
	inp=${@:OPTIND:2}
	xyz=$(echo $inp | awk '{print $1}') # xyz of structure to be tagged (needs to have proper xmol format)
	tag=$(echo $inp | awk '{print $2}') # xyz of tag (needs to have proper xmol format)
}

function usage () {
    echo "  tagit.sh [-t number_of_tags] [-c charge] [-m spin multiplicity] molecule.xyz tag.xyz"
    echo "  (default parameter is 1 tag)"
}

# define program directories
# these directories need to be adapted to your system
xtbdir="/cluster/home/pollicer/bin/xtb"
mwfndir="/cluster/home/pollicer/bin/multiwfn"
pydir="/cluster/home/pollicer/bin/tagit"
curdir=$(pwd)

# define color for echo
red=$(tput setaf 1)
green=$(tput setaf 2)
reset=$(tput sgr0)

get_args $ARGS
fpath_xyz=$(readlink -f $xyz)
fpath_tag=$(readlink -f $tag)
fdir=$(dirname $fpath_xyz)
fdir_test=$(dirname $fpath_xyz)

if [ $fdir != $fdir_test ]; then
	echo "${red}Molecule and Tag are not in the same folter. Exiting...${reset}"
	exit 1
fi

fname=$(basename $fpath_xyz)
name=${fname/'.xyz'}

# copy promolecular densities
cp -rf $mwfndir/examples/atomwfn $fdir/

# create mwfn input file
echo "5" > $fdir/tagit_mwfn.inp
echo "-1" >> $fdir/tagit_mwfn.inp
echo "1" >> $fdir/tagit_mwfn.inp
echo "3" >> $fdir/tagit_mwfn.inp
echo "0" >> $fdir/tagit_mwfn.inp
echo "12" >> $fdir/tagit_mwfn.inp
echo "4" >> $fdir/tagit_mwfn.inp
echo "1" >> $fdir/tagit_mwfn.inp
echo "4.0" >> $fdir/tagit_mwfn.inp
echo "2" >> $fdir/tagit_mwfn.inp
echo "2" >> $fdir/tagit_mwfn.inp
echo "2.00" >> $fdir/tagit_mwfn.inp
echo "0" >> $fdir/tagit_mwfn.inp
echo "2" >> $fdir/tagit_mwfn.inp
echo "-2" >> $fdir/tagit_mwfn.inp
echo "1" >> $fdir/tagit_mwfn.inp
echo "1" >> $fdir/tagit_mwfn.inp
echo "0.000001" >> $fdir/tagit_mwfn.inp # isovalue of surface
echo "3" >> $fdir/tagit_mwfn.inp
echo "4.00" >> $fdir/tagit_mwfn.inp # spacing between points (0.25 is default in mwfn)
echo "0" >> $fdir/tagit_mwfn.inp
echo "7" >> $fdir/tagit_mwfn.inp
echo "q" >> $fdir/tagit_mwfn.inp

# run mwfn
cd $fdir
echo "${green}Creating vdW surface using Multiwfn${reset}"
$mwfndir/Multiwfn $fpath_xyz < $fdir/tagit_mwfn.inp > $fdir/tagit_mwfn.out
mv $fdir/vtx.txt $fdir/tagit_vdw.txt
cd $curdir

# clean files
rm -rf $fdir/wfntmp
rm -rf $fdir/atomwfn
rm $fdir/tagit_mwfn.inp
rm $fdir/tagit_mwfn.out

# prepare python
cp $fpath_xyz $fdir/tagit_molecule.xyz
cp $fpath_tag $fdir/tagit_tag.xyz

# run python script
echo "${green}Generating Initial Tag Guesses${reset}"
$pydir/tagit_v001.py $fdir/ $tags

# clean files
rm $fdir/tagit_tag.xyz
rm $fdir/tagit_vdw.txt

# combine xyz files
cd $fdir
if [ -e ./xtbscreen.xyz ]
then
	rm ./xtbscreen.xyz
fi
touch ./xtbscreen.xyz
for i in $(ls | grep "tagit_" | grep "\.xyz" | grep -v "tagit_molecule.xyz");
do
	cat ./xtbscreen.xyz $i > tagit_temp.xyz
	mv ./tagit_temp.xyz ./xtbscreen.xyz
	rm $i
done

# run xtb to screen geometries
# export enviroment variables for xtb
export OMP_NUM_THREADS=$nproc
export MKL_NUM_THREADS=$nproc
export OMP_STACKSIZE="${mem}M"

# perform initial optimization
echo "${green}Reoptimize structures using xtb${reset}"
xtb ./tagit_molecule.xyz --screen --gfn 1 --chrg $charge --uhf $((spin-1)) > ./tagit_xtbscreen.out
mv ./xtbscreen.log ./tagit_screen.xyz

# clean files
for i in $(ls | grep "coord\."); do rm $i; done
rm $fdir/tagit_molecule.xyz
rm $fdir/charges
rm $fdir/xtbscreen.xyz
rm $fdir/xtbopt.xyz
rm $fdir/wbo
rm $fdir/xtbopt.log
rm $fdir/xtbrestart

