# tagit

Scripts to investigate van der Waals (vdW) complexes of analyte molecules with small gas molecules. The gas molecules are placed systematically on different positions of the vdW surface of the analyte.

# Usage

The paths to the external programs need to be adjusted before running the scripts. For the following examples, it is assumed that current working directory is in the examples folder and the relative paths are equal to the repository after extracting the ZIP archives.

## Example - Tagging of furan with a single dihydrogen molecule

`../tagit.sh -t 1 -c 0 -m 1 ./furan.xyz h2.xyz`